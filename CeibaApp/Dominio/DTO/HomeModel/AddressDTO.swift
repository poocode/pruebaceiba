//
//  AddressDTO.swift
//  CeibaApp
//
//  Created by German Garcia on 1/11/20.
//

import Foundation
import ObjectMapper

class AddressDTO: NSObject, Mappable {
    
    var street: String?
    var suite: String?
    var city: String?
    var zipcode: String?
    var geo: GeoDTO?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        street <- map["street"]
        suite <- map["suite"]
        city <- map["city"]
        zipcode <- map["zipcode"]
        geo <- map["geo"]
    }
    
}

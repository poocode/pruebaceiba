//
//  Company.swift
//  CeibaApp
//
//  Created by German Garcia on 1/11/20.
//

import Foundation
import ObjectMapper

class CompanyDTO: NSObject, Mappable {
    
    var name: String?
    var catchPhrase: String?
    var bs: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        catchPhrase <- map["catchPhrase"]
        bs <- map["bs"]
    }
    
}

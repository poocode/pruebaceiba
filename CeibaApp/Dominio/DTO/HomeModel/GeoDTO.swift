//
//  GeoDTO.swift
//  CeibaApp
//
//  Created by German Garcia on 1/11/20.
//

import Foundation
import ObjectMapper

class GeoDTO: NSObject, Mappable {
    
    var lat: String?
    var lng: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
    }
    
}

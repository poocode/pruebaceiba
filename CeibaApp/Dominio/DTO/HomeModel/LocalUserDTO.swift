//
//  LocalUserDTO.swift
//  CeibaApp
//
//  Created by German Garcia on 2/11/20.
//

import Foundation
import RealmSwift

class LocalUserDTO: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var phone = ""
    @objc dynamic var email = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

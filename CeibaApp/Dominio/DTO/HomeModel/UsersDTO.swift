//
//  UsersDTO.swift
//  CeibaApp
//
//  Created by German Garcia on 1/11/20.
//

import Foundation
import ObjectMapper

class UsersDTO: NSObject, Mappable {
    
    var id: Int?
    var name: String?
    var username: String?
    var email: String?
    var address: AddressDTO?
    var phone: String?
    var website: String?
    var company: CompanyDTO?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        username <- map["username"]
        email <- map["email"]
        address <- map["address"]
        phone <- map["phone"]
        website <- map["website"]
        company <- map["company"]
    }
    
}

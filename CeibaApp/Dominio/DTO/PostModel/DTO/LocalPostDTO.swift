//
//  LocalPostDTO.swift
//  CeibaApp
//
//  Created by German Garcia on 2/11/20.
//

import Foundation
import RealmSwift

class LocalPostDTO: Object {
    
    @objc dynamic var userId = 0
    @objc dynamic var id = 0
    @objc dynamic var title = ""
    @objc dynamic var body = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

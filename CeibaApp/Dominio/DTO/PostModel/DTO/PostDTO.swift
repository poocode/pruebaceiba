//
//  PostDTO.swift
//  CeibaApp
//
//  Created by German Garcia on 2/11/20.
//

import Foundation
import ObjectMapper

class PostDTO: NSObject, Mappable {
    
    var userId: Int?
    var id: Int?
    var title: String?
    var body: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        userId <- map["userId"]
        id <- map["id"]
        title <- map["title"]
        body <- map["body"]
    }
    
}

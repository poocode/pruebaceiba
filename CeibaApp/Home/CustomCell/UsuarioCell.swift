//
//  UsuarioCell.swift
//  CeibaApp
//
//  Created by German Garcia on 1/11/20.
//

import UIKit

protocol ActionClic {
    func actionCell(at index:IndexPath)
}

class UsuarioCell: UITableViewCell {
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtPhone: UILabel!
    @IBOutlet weak var txtMail: UILabel!
    @IBOutlet weak var txtArticle: UILabel!
    
    
    var delegate: ActionClic!
    var indexPath: IndexPath!
    
    var users: LocalUserDTO! {
           didSet {
               self.fileList()
           }
    }
       
    private func fileList() {
        viewContent.layer.cornerRadius = CGFloat(10)
        viewContent.clipsToBounds = true
        viewContent.layer.borderWidth = CGFloat(1)
        viewContent.layer.borderColor = UIColor(named: "divider")!.cgColor
        
        txtName.text = users.name
        txtPhone.text = users.phone
        txtMail.text = users.email
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.someActionClic (_:)))
        self.addGestureRecognizer(gesture)
    }
    
    @objc func someActionClic(_ sender:UITapGestureRecognizer){
        self.delegate?.actionCell(at: indexPath)
    }

}

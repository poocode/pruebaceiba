//
//  HomeInteractor.swift
//  CeibaApp
//
//  Created by German Garcia on 1/11/20.
//  
//

import Foundation
import RealmSwift

class HomeInteractor: HomeInteractorInputProtocol {

    // MARK: Properties
    weak var presenter: HomeInteractorOutputProtocol?
    var localDatamanager: HomeLocalDataManagerInputProtocol?
    var remoteDatamanager: HomeRemoteDataManagerInputProtocol?
    
    func getUserListPresenter() {
        
        var users: Results<LocalUserDTO> {
            let realm = try! Realm()
            return realm.objects(LocalUserDTO.self)
        }
        
        if users.count > 0 {
            var localUserDTOList = [LocalUserDTO]()
            for user in users {
                let localuser = LocalUserDTO()
                localuser.id = user.id
                localuser.name = user.name
                localuser.phone = user.phone
                localuser.email = user.email
                localUserDTOList.append(localuser)
            }
            presenter?.answerUserListPresenter(with: localUserDTOList)
        } else {
            remoteDatamanager?.getUserListIterator()
        }
        
    }
    
    func getPostUser(with parameter: LocalUserDTO) {
        
        var post: Results<LocalPostDTO> {
            let realm = try! Realm()
            return realm.objects(LocalPostDTO.self).sorted(byKeyPath: "title", ascending: true).filter("userId = \(parameter.id)")
        }
        
        if post.count > 0 {
            presenter?.answerResponsePostIterator(with: parameter.id)
        } else {
            remoteDatamanager?.getPostUser(with: parameter)
        }
    }
}

extension HomeInteractor: HomeRemoteDataManagerOutputProtocol {

    func answerUserListRemote(with response: [UsersDTO]) {
        var localUserDTOList = [LocalUserDTO]()
        let realm = try! Realm()
        
        for user in response {
            let localuser = LocalUserDTO()
            localuser.id = user.id!
            localuser.name = user.name!
            localuser.phone = user.phone!
            localuser.email = user.email!
            localUserDTOList.append(localuser)
        }
        
        try! realm.write {
            realm.add(localUserDTOList)
        }
        
        presenter?.answerUserListPresenter(with: localUserDTOList)
    }
    
    func callBackResponseError(with error: String) {
        presenter?.callBackResponseError(with: error)
    }
    
    func answerResponsePost(with response: [PostDTO], idUser: Int) {
   
        var localPostList = [LocalPostDTO]()
        let realm = try! Realm()
        
        for postData in response {
            let post = LocalPostDTO()
            post.id = postData.id!
            post.userId = postData.userId!
            post.title = postData.title!
            post.body = postData.body!
            localPostList.append(post)
        }
        
        try! realm.write {
            realm.add(localPostList)
        }
        
        presenter?.answerResponsePostIterator(with: idUser)
    }
    
}

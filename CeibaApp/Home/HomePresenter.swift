//
//  HomePresenter.swift
//  CeibaApp
//
//  Created by German Garcia on 1/11/20.
//  
//

import Foundation

class HomePresenter  {
    
    // MARK: Properties
    weak var view: HomeViewProtocol?
    var interactor: HomeInteractorInputProtocol?
    var wireFrame: HomeWireFrameProtocol?
    
}

extension HomePresenter: HomePresenterProtocol {
    // TODO: implement presenter methods
    
    func viewDidLoad() {
    }
    
    func closeAnimation(time: Double) {
        Timer.scheduledTimer(withTimeInterval: time, repeats: false) { (timer) in
          // sending notif here
          NotificationCenter.default.post(name: heartAttackNotificationName, object: nil)
        }
    }
    
    func getUserListView() {
        interactor?.getUserListPresenter()
    }
    
    func showViewDetail(with user: LocalUserDTO) {
        interactor?.getPostUser(with: user)
    }
}

extension HomePresenter: HomeInteractorOutputProtocol {
    
    func answerUserListPresenter(with response: [LocalUserDTO]) {
        view?.answerUserListView(with: response)
    }
    
    func callBackResponseError(with error: String) {
        view?.callBackResponseError(with: error)
    }
    
    
    func answerResponsePostIterator(with parameter: Int) {
        view?.finalizePostRequest()
        wireFrame?.presenterNewViewDetail(from: view!, parameter: parameter)
    }

}

//
//  HomeProtocols.swift
//  CeibaApp
//
//  Created by German Garcia on 1/11/20.
//  
//

import Foundation
import UIKit

protocol HomeViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: HomePresenterProtocol? { get set }
    
    func answerUserListView(with response: [LocalUserDTO])
    func callBackResponseError(with error: String)
    func finalizePostRequest()
}

protocol HomeWireFrameProtocol: class {
    // PRESENTER -> WIREFRAME
    static func createHomeModule() -> UIViewController
    func presenterNewViewDetail(from view: HomeViewProtocol, parameter: Int)
}

protocol HomePresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: HomeViewProtocol? { get set }
    var interactor: HomeInteractorInputProtocol? { get set }
    var wireFrame: HomeWireFrameProtocol? { get set }
    
    func viewDidLoad()
    func closeAnimation(time: Double)
    func getUserListView()
    func showViewDetail(with user: LocalUserDTO)
}

protocol HomeInteractorOutputProtocol: class {
    // INTERACTOR -> PRESENTER
    func answerUserListPresenter(with response: [LocalUserDTO])
    func callBackResponseError(with error: String)
    func answerResponsePostIterator(with parameter: Int)
}

protocol HomeInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: HomeInteractorOutputProtocol? { get set }
    var localDatamanager: HomeLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: HomeRemoteDataManagerInputProtocol? { get set }
    
    func getUserListPresenter()
    func getPostUser(with parameter: LocalUserDTO)
}

protocol HomeDataManagerInputProtocol: class {
    // INTERACTOR -> DATAMANAGER
}

protocol HomeRemoteDataManagerInputProtocol: class {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: HomeRemoteDataManagerOutputProtocol? { get set }
    func getUserListIterator()
    func getPostUser(with parameter: LocalUserDTO)
}

protocol HomeRemoteDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
    func answerUserListRemote(with response: [UsersDTO])
    func callBackResponseError(with error: String)
    func answerResponsePost(with response: [PostDTO], idUser: Int)
}

protocol HomeLocalDataManagerInputProtocol: class {
    // INTERACTOR -> LOCALDATAMANAGER
    /*func saveObject<T: Object>(object: T)
    func getObjects<T: Object>()->[T]
    func getObjectsFilter<T: Object>(filter:String)->[T]*/
}

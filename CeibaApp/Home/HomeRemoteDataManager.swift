//
//  HomeRemoteDataManager.swift
//  CeibaApp
//
//  Created by German Garcia on 1/11/20.
//  
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class HomeRemoteDataManager:HomeRemoteDataManagerInputProtocol {

    var remoteRequestHandler: HomeRemoteDataManagerOutputProtocol?
    
    func getUserListIterator() {
        
        Alamofire.request("\(Constant.PARAMETERS.ENDPOINT)\("users")", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseArray(completionHandler: { (response: DataResponse<[UsersDTO]>) in
            switch response.result {
            case .success:
                self.remoteRequestHandler?.answerUserListRemote(with: response.result.value!)
            case .failure(let error):
                self.remoteRequestHandler?.callBackResponseError(with: error.localizedDescription)
            }
        })
    }
    
    func getPostUser(with parameter: LocalUserDTO) {
        Alamofire.request("\(Constant.PARAMETERS.ENDPOINT)\("posts?userId=")\(String(describing: parameter.id))", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseArray(completionHandler: { (response: DataResponse<[PostDTO]>) in
            switch response.result {
            case .success:
                self.remoteRequestHandler?.answerResponsePost(with: response.result.value!, idUser: parameter.id)
            case .failure(let error):
                self.remoteRequestHandler?.callBackResponseError(with: error.localizedDescription)
            }
        })
    }
    
}

//
//  HomeView.swift
//  CeibaApp
//
//  Created by German Garcia on 1/11/20.
//  
//

import Foundation
import UIKit

class HomeView: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, ActionClic {

    // MARK: Properties
    var presenter: HomePresenterProtocol?
    var listUsers = [LocalUserDTO]()
    var filterListUsers: [LocalUserDTO]!
    
    // MARK: Lifecycle
    @IBOutlet weak var tableUsers: UITableView!
    @IBOutlet var searchBarView: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.closeAnimation(time: 3)
        validateUserData()
        
        searchBarView.showsCancelButton = true
        searchBarView.delegate = self

        navigationItem.titleView = searchBarView
        tableUsers.register(UINib.init(nibName: "UsuarioCell", bundle: nil), forCellReuseIdentifier: "UsuarioCell")
    }
    
    func validateUserData() {
        showProgress()
        presenter?.getUserListView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterListUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsuarioCell", for: indexPath) as! UsuarioCell
        cell.users = filterListUsers[indexPath.row]
        cell.delegate = self
        cell.indexPath = indexPath
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 138
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Do some search stuff
        print(searchText)
        filterListUsers = []
        
        if searchText == "" {
            filterListUsers = listUsers
        } else {
            for user in listUsers {
                if user.name.lowercased().contains(searchText.lowercased()) {
                    filterListUsers.append(user)
                }
            }
            
            if filterListUsers.count > 0 {
                tableUsers.isHidden = false
            } else {
                tableUsers.isHidden = true
            }
        }
        
        tableUsers.reloadData()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.endEditing(true)
    }
    
    func actionCell(at index: IndexPath) {
        showProgress()
        presenter?.showViewDetail(with: filterListUsers[index.row])
    }
    
}

extension HomeView: HomeViewProtocol {

    func finalizePostRequest() {
        hideProgress()
    }
    
    func answerUserListView(with response: [LocalUserDTO]) {
        self.listUsers = response
        self.filterListUsers = self.listUsers
        tableUsers.reloadData()
        hideProgress()
    }
    
    func callBackResponseError(with error: String) {
        hideProgress()
        showAlerts(title: Constant.MESSAGES.TITLEALERVIEW, message: error)
    }
}


//
//  BaseViewController.swift
//  CeibaApp
//
//  Created by German Garcia on 1/11/20.
//

import UIKit
import JGProgressHUD

class BaseViewController: UIViewController {

    var hud: JGProgressHUD?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTouchGesture))
        view.addGestureRecognizer(tap)
        
        if self.traitCollection.userInterfaceStyle == .dark {
            // User Interface is Dark
            hud = JGProgressHUD(style: .dark)
        } else {
            // User Interface is Light
            hud = JGProgressHUD(style: .light)
        }
        
    }
    
    func showAlerts(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: Constant.MESSAGES.DONE, style: .default) { (action:UIAlertAction) in
            
        }
        
        alert.addAction(action1)
        present(alert, animated: true, completion: nil)
    }
    
    func showProgress() {
        hud!.vibrancyEnabled = false
        hud!.textLabel.text = Constant.MESSAGES.LOADING
        hud!.show(in: self.view)
    }
    
    func hideProgress() {
        hud!.dismiss(afterDelay: 0.1)
    }
    
    @objc func onTouchGesture(){
        view.endEditing(true)
    }
    
}

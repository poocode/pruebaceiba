//
//  PostCell.swift
//  CeibaApp
//
//  Created by German Garcia on 2/11/20.
//

import UIKit

class PostCell: UITableViewCell {

    @IBOutlet weak var txtTitlr: UILabel!
    @IBOutlet weak var txtBody: UILabel!
    
    var post: LocalPostDTO! {
           didSet {
               self.fileList()
           }
    }
    
    private func fileList() {
        txtTitlr.text = post.title
        txtBody.text = post.body
    }
    
}

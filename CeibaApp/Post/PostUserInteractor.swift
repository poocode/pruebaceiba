//
//  PostUserInteractor.swift
//  CeibaApp
//
//  Created by German Garcia on 2/11/20.
//  
//

import Foundation
import RealmSwift

class PostUserInteractor: PostUserInteractorInputProtocol {
    
    // MARK: Properties
    weak var presenter: PostUserInteractorOutputProtocol?
    var localDatamanager: PostUserLocalDataManagerInputProtocol?
    var remoteDatamanager: PostUserRemoteDataManagerInputProtocol?

    func setUserDatos(userId: Int) {
        
        let realm = try! Realm()
        let user = realm.objects(LocalUserDTO.self).filter("id = \(String(describing: userId))").first
        
        presenter?.showDataUserHeader(user: user!)
    }
    
    func setPostData(userId: Int) {
        let realm = try! Realm()
        
        let listPost = realm.objects(LocalPostDTO.self).sorted(byKeyPath: "userId", ascending: true).filter("userId = \(userId)")
        
        var localPostList = [LocalPostDTO]()
        
        for postData in listPost {
            let post = LocalPostDTO()
            post.id = postData.id
            post.userId = postData.userId
            post.title = postData.title
            post.body = postData.body
            localPostList.append(post)
        }
        
        presenter?.showListPost(listPost: localPostList)
    }
}

extension PostUserInteractor: PostUserRemoteDataManagerOutputProtocol {
    // TODO: Implement use case methods
}

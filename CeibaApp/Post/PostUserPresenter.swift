//
//  PostUserPresenter.swift
//  CeibaApp
//
//  Created by German Garcia on 2/11/20.
//  
//

import Foundation
import RealmSwift

class PostUserPresenter  {
    
    // MARK: Properties
    weak var view: PostUserViewProtocol?
    var interactor: PostUserInteractorInputProtocol?
    var wireFrame: PostUserWireFrameProtocol?
    var parameterId: Int?
}

extension PostUserPresenter: PostUserPresenterProtocol {
    
    func setPostData() {
        interactor?.setPostData(userId: parameterId!)
    }
    
    func setHeaderUser() {
        interactor?.setUserDatos(userId: parameterId!)
    }
    
    // TODO: implement presenter methods
    func viewDidLoad() {
    }
}

extension PostUserPresenter: PostUserInteractorOutputProtocol {
    
    func showListPost(listPost: [LocalPostDTO]) {
        view?.showPostList(listPost: listPost)
    }
    
    func showDataUserHeader(user: LocalUserDTO) {
        view?.showDataUser(user: user)
    }
    // TODO: implement interactor output methods
}

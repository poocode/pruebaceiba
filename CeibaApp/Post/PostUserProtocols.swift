//
//  PostUserProtocols.swift
//  CeibaApp
//
//  Created by German Garcia on 2/11/20.
//  
//

import Foundation
import UIKit

protocol PostUserViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: PostUserPresenterProtocol? { get set }
    func showDataUser(user: LocalUserDTO)
    func showPostList(listPost: [LocalPostDTO])
}

protocol PostUserWireFrameProtocol: class {
    // PRESENTER -> WIREFRAME
    static func createPostUserModule(parameter: Int) -> UIViewController
}

protocol PostUserPresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: PostUserViewProtocol? { get set }
    var interactor: PostUserInteractorInputProtocol? { get set }
    var wireFrame: PostUserWireFrameProtocol? { get set }
    var parameterId: Int? { get set }
    
    func viewDidLoad()
    func setHeaderUser()
    func setPostData()
}

protocol PostUserInteractorOutputProtocol: class {
    // INTERACTOR -> PRESENTER
    func showDataUserHeader(user: LocalUserDTO)
    func showListPost(listPost: [LocalPostDTO])
}

protocol PostUserInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: PostUserInteractorOutputProtocol? { get set }
    var localDatamanager: PostUserLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: PostUserRemoteDataManagerInputProtocol? { get set }
    
    func setUserDatos(userId: Int)
    func setPostData(userId: Int)
}

protocol PostUserDataManagerInputProtocol: class {
    // INTERACTOR -> DATAMANAGER
}

protocol PostUserRemoteDataManagerInputProtocol: class {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: PostUserRemoteDataManagerOutputProtocol? { get set }
}

protocol PostUserRemoteDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
}

protocol PostUserLocalDataManagerInputProtocol: class {
    // INTERACTOR -> LOCALDATAMANAGER
}

//
//  PostUserView.swift
//  CeibaApp
//
//  Created by German Garcia on 2/11/20.
//  
//

import Foundation
import UIKit

class PostUserView: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var txtname: UILabel!
    @IBOutlet weak var txtMail: UILabel!
    @IBOutlet weak var txtPhone: UILabel!
    @IBOutlet weak var tablePost: UITableView!
    
    // MARK: Properties
    var presenter: PostUserPresenterProtocol?
    var listPost = [LocalPostDTO]()
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.setHeaderUser()
        tablePost.register(UINib.init(nibName: "PostCell", bundle: nil), forCellReuseIdentifier: "PostCell")
        presenter?.setPostData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        cell.post = listPost[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 124
    }
    
}

extension PostUserView: PostUserViewProtocol {
    func showPostList(listPost: [LocalPostDTO]) {
        self.listPost = listPost
        tablePost.reloadData()
    }
    
    
    func showDataUser(user: LocalUserDTO) {
        txtname.text = user.name
        txtMail.text = user.email
        txtPhone.text = user.phone
    }
    
}

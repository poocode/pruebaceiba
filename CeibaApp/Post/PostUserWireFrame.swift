//
//  PostUserWireFrame.swift
//  CeibaApp
//
//  Created by German Garcia on 2/11/20.
//  
//

import Foundation
import UIKit

class PostUserWireFrame: PostUserWireFrameProtocol {

    class func createPostUserModule(parameter: Int) -> UIViewController {
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "postUserView")
        if let view = viewController as? PostUserView {
            let presenter: PostUserPresenterProtocol & PostUserInteractorOutputProtocol = PostUserPresenter()
            let interactor: PostUserInteractorInputProtocol & PostUserRemoteDataManagerOutputProtocol = PostUserInteractor()
            let localDataManager: PostUserLocalDataManagerInputProtocol = PostUserLocalDataManager()
            let remoteDataManager: PostUserRemoteDataManagerInputProtocol = PostUserRemoteDataManager()
            let wireFrame: PostUserWireFrameProtocol = PostUserWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            presenter.parameterId = parameter
            interactor.presenter = presenter
            interactor.localDatamanager = localDataManager
            interactor.remoteDatamanager = remoteDataManager
            remoteDataManager.remoteRequestHandler = interactor
            
            return viewController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "PostUserView", bundle: Bundle.main)
    }
    
}

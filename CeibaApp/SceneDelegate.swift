//
//  SceneDelegate.swift
//  CeibaApp
//
//  Created by German Garcia on 31/10/20.
//

import UIKit
import RevealingSplashView

let heartAttackNotificationName = Notification.Name("heartAttack")

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "ceiba_logo")!,iconInitialSize: CGSize(width: 250, height: 250), backgroundColor: UIColor(named: "primary")!)

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
       
        // Initialize VIPER module to show a window with this ViewController
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            let homeView = HomeWireFrame.createHomeModule()
            window.rootViewController = homeView
            self.window = window
            window.makeKeyAndVisible()
        }
        
        revealingSplashView.animationType = .heartBeat
        window?.addSubview(revealingSplashView)
        revealingSplashView.startAnimation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleHeartAttack), name: heartAttackNotificationName, object: nil)
        
    }

    @objc func handleHeartAttack() {
      revealingSplashView.heartAttack = true
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
    }

    func sceneWillResignActive(_ scene: UIScene) {
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
    }

}


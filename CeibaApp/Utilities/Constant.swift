//
//  Constant.swift
//  CeibaApp
//
//  Created by German Garcia on 1/11/20.
//

import Foundation

struct Constant {
    
    struct PARAMETERS {
        static let ENDPOINT = "https://jsonplaceholder.typicode.com/"
    }
    
    struct MESSAGES {
        static let BUTTONTITLEACCEPT = "OK"
        static let TITLEALERVIEW = "Ups!"
        static let LOADING = "loading"
        static let DONE = "Done"
        static let CANCEL = "Cancel"
        
        
    }
}

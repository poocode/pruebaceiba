//
//  CeibaAppTests.swift
//  CeibaAppTests
//
//  Created by German Garcia on 31/10/20.
//

import XCTest
@testable import CeibaApp

class CeibaAppTests: XCTestCase {
    
    var homepresenter: HomePresenter?
    var myDatesViewController : HomeView?
    
    override func setUp() {
        super.setUp()
        
        let storyboard1 = UIStoryboard(name: "HomeView", bundle: Bundle(for: HomeView.self))
        myDatesViewController = storyboard1.instantiateViewController(withIdentifier: "homeView") as? HomeView
        let _ = myDatesViewController!.view
        
        homepresenter = HomePresenter()
        
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testValidateViewServices() throws {
        XCTAssertNotNil(myDatesViewController?.viewDidLoad())
    }
    
    func testServiceUser() throws {
        XCTAssertNotNil(myDatesViewController?.validateUserData())
    }

    func testServiceResponse() throws {
        let listUsers = [LocalUserDTO]()
        XCTAssertNotNil(myDatesViewController?.answerUserListView(with: listUsers))
        
        XCTAssertNotNil(homepresenter?.getUserListView())
    }
}

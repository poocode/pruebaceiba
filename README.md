## Arquitectura utilizada VIPER

VIPER es un acrónimo de **View**, **Interactor**, **Presenter**, **Entity** y **Router**. Esta arquitectura está basada en Principios de Responsabilidad Unica, que guía hacia una arquitectura más limpia, para tener una estructura mejor [VIPER]([https://apiumhub.com/es/tech-blog-barcelona/arquitectura-viper/](https://apiumhub.com/es/tech-blog-barcelona/arquitectura-viper/)). 

```mermaid

graph TD
A(VIEW)  --- B(PRESENTER)
B --- C(WIREFRAME)
B --- D(ITERACTOR)
D --- E(REMOTE DATA MANAGER)
D --- G(LOCAL DATA MANAGER)
D --- F(ENTITY)
```

## Diagrama  UML 

La finalidad de los diagramas es presentar diversas perspectivas de un sistema, a las cuales se les conoce como modelo.

```mermaid

sequenceDiagram
VIEW ->> PRESENTER: Fecha
PRESENTER-->>ITERACTOR: Solicitud

ITERACTOR-->> REMOTE: Llamado al servicio Rest
REMOTE-->> ITERACTOR: Respuesta ok
ITERACTOR-->> PRESENTER: Serializacion
PRESENTER ->> VIEW: Llenar tabla con los datos
REMOTE--x ITERACTOR: Respuesta Error
ITERACTOR--x PRESENTER: Error servicio
PRESENTER --x VIEW: Mostrar dialogo de error
```

## Control de versiones GITFLOW 

El flujo de trabajo de Gitflow es un diseño de flujo de trabajo de Git que fue publicado por primera vez y popularizado por [Vincent Driessen en nvie](http://nvie.com/posts/a-successful-git-branching-model/). El flujo de trabajo de Gitflow define un modelo estricto de ramificación diseñado alrededor de la publicación del proyecto. Proporciona un marco sólido para gestionar proyectos más grandes.

![enter image description here](https://3.bp.blogspot.com/-_tuoCwk7YWw/WkHM4Dep2ZI/AAAAAAAACA4/dxbaMtFhHrUjL1vIkL-Ujlq5VFTpYYAqACLcBGAs/s1600/04%2B%25281%2529.png)


## Clean Code: código limpio

Clean Code, o Código Limpio, es una filosofía de desarrollo de software que consiste en aplicar **técnicas simples que facilitan la escritura y lectura de un código**, volviéndolo más fácil de entender.


## Librerías utilizadas

- **`'Alamofire', '~>  4.8.0'.`**
	> Esta es una librería especializada para consumir servicios **REST** y **SOAP** de una forma asincrónica y fácil de implementar

- **`'AlamofireObjectMapper', '~> 5.2'.`**
	> Esta librería es la encargada de serializar los datos recibidos de una forma **muy sencilla**

- **'`JGProgressHUD'.`**
	> Con esta librería mostramos por pantalla un dialogo de cargando información, mientras el servicio realiza la petición al API
